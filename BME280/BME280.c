#define BME280 0x76      //The I2C Address of the Device


void Write_BME_Register(int,int,int);
int Read_BME_Register(int,int);
void get_dataBME280();

void init_BME280();
float get_Temp();
float get_Hum();
float get_Pess();

float Temperature;
signed int32 t_fine;
int32 Pressure;

int32 rawpres;
int32 rawtemp;
int32 rawhum;


int16 Dig_T1;
signed int16 Dig_T2;
signed int16 Dig_T3;

int16 Dig_H1;
signed int16 Dig_H2;
int16 Dig_H3;
signed int16 Dig_H4;
signed int16 Dig_H5;
signed int16 Dig_H6;

int16 Dig_P1;
signed int16 Dig_P2;
signed int16 Dig_P3;
signed int16 Dig_P4;
signed int16 Dig_P5;
signed int16 Dig_P6;
signed int16 Dig_P7;
signed int16 Dig_P8;
signed int16 Dig_P9;

void init_BME280()
{   
   Write_BME_Register(BME280,0xF4,0x00);   //Put it in sleep mode
   delay_ms(1000);
   Write_BME_Register(BME280,0xF5,0x84);   //Set Config Word
   Write_BME_Register(BME280,0xF2,0x04);   //Set Humidity Control
   Write_BME_Register(BME280,0xF4,0x93);   //Set Temp & Press Control + Mode

   Dig_T1 = make16 (Read_BME_Register(BME280,0x89),Read_BME_Register(BME280,0x88));
   Dig_T2 = make16 (Read_BME_Register(BME280,0x8B),Read_BME_Register(BME280,0x8A));
   Dig_T3 = make16 (Read_BME_Register(BME280,0x8D),Read_BME_Register(BME280,0x8C));

   Dig_H1 = Read_BME_Register(BME280,0xA1);
   Dig_H2 = make16 (Read_BME_Register(BME280,0xE2),Read_BME_Register(BME280,0xE1));
   Dig_H3 = Read_BME_Register(BME280,0xE3);

   Dig_H4=(((int16)Read_BME_Register(BME280,0xE4)<<4)|(Read_BME_Register(BME280,0xE5)& 0x0F)); //SO FAR BEST
   Dig_H5 = make16((Read_BME_Register(BME280,0xE5)>>4), Read_BME_Register(BME280,0xE6));
   Dig_H6 = Read_BME_Register(BME280,0xE7);

   Dig_P1 = make16 (Read_BME_Register(BME280,0x8F),Read_BME_Register(BME280,0x8E));
   Dig_P2 = make16 (Read_BME_Register(BME280,0x91),Read_BME_Register(BME280,0x90));
   Dig_P3 = make16 (Read_BME_Register(BME280,0x93),Read_BME_Register(BME280,0x92));
   Dig_P4 = make16 (Read_BME_Register(BME280,0x95),Read_BME_Register(BME280,0x94));
   Dig_P5 = make16 (Read_BME_Register(BME280,0x97),Read_BME_Register(BME280,0x96));
   Dig_P6 = make16 (Read_BME_Register(BME280,0x99),Read_BME_Register(BME280,0x98));
   Dig_P7 = make16 (Read_BME_Register(BME280,0x9B),Read_BME_Register(BME280,0x9A));
   Dig_P8 = make16 (Read_BME_Register(BME280,0x9D),Read_BME_Register(BME280,0x9C));
   Dig_P9 = make16 (Read_BME_Register(BME280,0x9F),Read_BME_Register(BME280,0x9E));
}

void Write_BME_Register(int Dev_Read_Add, int Register_add, int Data) {
   //int readbit =1;
   //int writebit =0;
   int tempaddress = 0;

   tempaddress = Dev_Read_Add <<1;    //Left Shift 1 bit to allow space for R/W bit
   //tempaddress |= writebit;         //OR the shifted address with the WRITE Bit
   i2c_start();
   i2c_write(tempaddress);          //Address the device with shifted address with WRITE bit set
   i2c_write(Register_add);         //Tell the Device Which Register you want to read
   i2c_write(Data);   
   i2c_stop();
}

int Read_BME_Register(int Dev_Read_Add, int Register_add) {
   //int readbit=1;
   //int writebit=0;
   int tempaddress=0;
   int Result_Byte=0;

   tempaddress = Dev_Read_Add <<1;    //Left Shift 1 bit to allow space for R/W bit
//   tempaddress |= 0x00;         //OR the shifted address with the WRITE Bit

   i2c_start();                  //Start I2C sequence
   i2c_write(tempaddress);          //Address the device with shifted address with WRITE bit set
   i2c_write(Register_add);         //Tell the Device Which Register you want to read
   
//   tempaddress = Dev_Read_Add <<1;    //Left Shift 1 bit to allow space for R/W bit
   tempaddress |= 0x01;            //OR the shifted address with the READ Bit

   i2c_start();                  //Start I2C sequence
   i2c_write(tempaddress);          //Address the device with shifted address with READ bit set
   Result_Byte = i2c_read(0);         //READ the data onto a Variable - Device ID in this case
   i2c_stop();                     //Stop The I2C transaction
   
   //fprintf(lcd_putc,"Register:%X\n\r", Result_Byte);    //Print your Data --- For the Win.

   return(Result_Byte);
}

float get_Pess() {
   float var1, var2, p;
   
   var1 = ((float)t_fine/2.0) - 64000.0;
   var2 = var1 * var1 * ((float)Dig_P6) / 32768.0;
   var2 = var2 + var1 * ((float)Dig_P5) * 2.0;
   var2 = (var2/4.0)+(((float)Dig_P4) * 65536.0);
   var1 = (((float)Dig_P3) * var1 * var1 / 524288.0 + ((float)Dig_P2) * var1) / 524288.0;
   var1 = (1.0 + var1 / 32768.0)*((float)Dig_P1);
   if (var1 == 0.0)  return 0; // avoid exception caused by division by zero
   p = 1048576.0 - (float)rawpres;
   p = (p - (var2 / 4096.0)) * 6250.0 / var1;
   var1 = ((float)Dig_P9) * p * p / 2147483648.0;
   var2 = p * ((float)Dig_P8) / 32768.0;
   p = p + (var1 + var2 + ((float)Dig_P7)) / 16.0;
   return((p/100.0));
}

float get_Hum() {
   signed int32 var1;
   var1 = (t_fine - ((int32)76800));
   var1 = (((((rawhum << 14) - (((int32)Dig_H4) << 20) - (((int32)Dig_H5) * var1)) + ((int32)16384)) >> 15) * (((((((var1 * ((int32)Dig_H6)) >> 10) * (((var1 * ((int32)Dig_H3)) >> 11) + ((int32)32768))) >> 10) + ((int32)2097152)) *((int32)Dig_H2) + 8192) >> 14));
   var1 = (var1 - (((((var1 >> 15) * (var1 >> 15)) >> 7) * ((int32)Dig_H1)) >> 4));
   var1 = (var1 < 0 ? 0 : var1);
   var1 = (var1 > 419430400 ? 419430400 : var1);
   var1= var1 >> 12;
   return((float)var1/1024.0);
}

float get_Temp() {
   float var1, var2, T;
   var1 = (((float)rawtemp)/16384.0 - ((float)Dig_T1)/1024.0) * ((float)Dig_T2);
   var2 = ((((float)rawtemp)/131072.0 - ((float)Dig_T1)/8192.0) *
   (((float)rawtemp)/131072.0 - ((float) Dig_T1)/8192.0)) * ((float)Dig_T3);
   t_fine = (signed int32)(var1 + var2);
   Temperature = (var1 + var2) / 5120.0;
   return(Temperature);
}

void get_dataBME280() {
   int tempaddress = 0;
   int Temp[8];

   tempaddress = BME280 <<1;    //Left Shift 1 bit to allow space for R/W bit
//   tempaddress |= writebit;         //OR the shifted address with the WRITE Bit
   i2c_start();                  //Start I2C sequence
   i2c_write(tempaddress);          //Address the device with shifted address with WRITE bit set
   i2c_write(0xF7);         //Tell the Device Which Register you want to read
   
//   tempaddress = BME280 <<1;    //Left Shift 1 bit to allow space for R/W bit
   tempaddress |= 0x01;            //OR the shifted address with the READ Bit
   i2c_start();                  //Start I2C sequence
   i2c_write(tempaddress);          //Address the device with shifted address with READ bit set
   Temp[0]= i2c_read(1);            //0xF7
   Temp[1]= i2c_read(1);            //0xF8
   Temp[2]= i2c_read(1);            //0xF9
   Temp[3]= i2c_read(1);            //0xFa
   Temp[4]= i2c_read(1);            //0xFb
   Temp[5]= i2c_read(1);            //0xFc
   Temp[6]= i2c_read(1);            //0xFd
   Temp[7]= i2c_read(0);            //0xFe
   i2c_stop();                     //Stop The I2C transaction
   
   rawpres = make32(0x00, Temp[0], Temp[1],Temp[2]);
    rawpres >>= 4;

   rawtemp = make32(0x00, Temp[3], Temp[4],Temp[5]);
    rawtemp >>= 4;

   rawhum = make32(0x00,0x00,Temp[6],Temp[7]);

}