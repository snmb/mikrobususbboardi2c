#include "main.h"
#include "dui.c"
#include "./BME280.c" 


 int8 state = 0;
//============================================================
//TASK MESSAGE CHECK
#task(rate = 1000 ms, max = 300 us)
void task_check();

#task(rate = 10 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);

//============================================================
void task_check() {
   switch (state) {
      case 0:
         output_bit(LED1, 1);
         get_dataBME280();
         output_bit(LED1, 0);
         state = 1; 
         break;
      case 1:  
         DUISendInt16(0, (int16)get_Temp());
         state = 2; 
         break;
      case 2:  
         DUISendInt16(1, (int16)get_Pess());
         state = 3; 
         break;
      case 3:  
         DUISendInt16(2, (int16)get_Hum());
         state = 0; 
         break;
     default: state = 0; break;
    }
}

void task_DUIcheck() {
   DUICheck();
}

void formInit(void) {
   
   char text1[]="Pressure";
   char text2[]="kPa";
   DUIAddLabel(1, text1, text2);
   char text3[]="Temperature";
   char text4[]="C";
   DUIAddLabel(0, text3, text4);
   char suffix[]="%";
   char textb0[]="Hum";
   DUIAddLabel(2, textb0, suffix);   


}

void formRead(unsigned int8 channel, unsigned int8* data) {

   // if (channel == 4) {
   // //   Temperature = DUIReadInt16(1, data);
   // }

}

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x80 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);
    init_BME280();
}

void main() {
   initialization();
   rtos_run();
}
