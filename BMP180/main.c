#include "main.h"
#include "dui.c"
//#include "./BMP180_.c" 


// unsigned int16 Humidity = 0;
// signed int16 Temperature = 0;
 int8 state = 0;
// unsigned char data_buffer[8]; 
//===================== I2C ===================================
unsigned int8 i2cState = 0;
//------------------ BMP180 -----------------------------------
#define BMP180_ADDR 0xEE
// floating point cal factors 
float BMP180_fc4, BMP180_fc5, BMP180_fc6, BMP180_fmc, BMP180_fmd; 
// polynomomial constants 
float BMP180_x0, BMP180_x1, BMP180_x2, BMP180_y0, BMP180_y1, BMP180_y2; 
float BMP180_s;     // T-25, used in pressure calculation - must run temperature reading before pressure reading 
float BMP180_Temp;  // set after every temperature or temperature/pressure reading 
unsigned int8 BMP180_Temp_H;
unsigned int8 BMP180_Temp_L;

float BMP180_alpha;
int8 BMP180_msb, BMP180_lsb, BMP180_xlsb; 
float BMP180_pu;
float BMP180_x, BMP180_y, BMP180_z; 
float BMP180_Press;

int8 BMP180_ReadByte(int8 address);
int16 BMP180_ReadInt(int8 address);
void BMP180_WriteByte(int8 address, int8 data);


//------------------ I2C TASK ---------------------------------
#task(rate = 100 ms, max = 600 us)
void task_I2Ccheck();

//============================================================
float Tf, P_inHg; 
float T_Cent; 
float P_mBar; 
int16 tp, tt;

//TASK MESSAGE CHECK
#task(rate = 1000 ms, max = 300 us)
void task_check();

#task(rate = 10 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);

//============================================================
void task_check() {
   switch (state) {
      case 0:
         // output_bit(LED1, 1);
         DUISendUInt32(0, BMP180_Press);
         DUISendTemperature(1, BMP180_Temp_H, BMP180_Temp_L);
         // output_bit(LED1, 0);
         break;
      case 2:  

         break;
  
     default: state = 0; break;
   
    }
}

void task_DUIcheck() {
   DUICheck();
}

void formInit(void) {
   
   char text1[]="BMP180 Давл.";
   char text2[]="Pa";
   DUIAddLabel(0, text1, text2);
   char text3[]="BMP180 Темп.";
   char text4[]="C";
   DUIAddLabel(1, text3, text4);
   //  char text5[]="Temperature (type t)";
   // char text6[]="C";
   // DUIAddLabel(2, text5, text6);  
   // char suffix[]="";
   // char textb0[]="b0";
   //  DUIAddLabel(2, textb0, suffix);   
   // char textb1[]="b1";
   //  DUIAddLabel(3, textb1, suffix);
   // char textb2[]="b2";
   //  DUIAddLabel(4, textb2, suffix);
   // char textb3[]="b3";
   //  DUIAddLabel(5, textb3, suffix);   
   // char textb4[]="b4";
   //  DUIAddLabel(6, textb4, suffix);
   // char textb5[]="b5";
   //  DUIAddLabel(7, textb5, suffix);  
   // char textb6[]="b6";
   //  DUIAddLabel(8, textb6, suffix);
   // char textb7[]="b7";
   //  DUIAddLabel(9, textb7, suffix);

}

void formRead(unsigned int8 channel, unsigned int8* data) {

   // if (channel == 4) {
   // //   Temperature = DUIReadInt16(1, data);
   // }

}

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x80 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);
  //  BMP180_Calibration();
}

void main() {
   initialization();
   rtos_run();
}
  // output_bit(Logic_PGD, 1);
//===================== I2C ===================================
//------------------ BMP180 -----------------------------------
int8 BMP180_ReadByte(int8 address) {
   int8 data; 
   i2c_start(); 
   i2c_write(BMP180_ADDR); 
   i2c_write(address); 
   i2c_start(); 
   i2c_write(BMP180_ADDR | 0x01 ); 
   data=i2c_read(0); 
   i2c_stop(); 
   return(data); 
}

int16 BMP180_ReadInt(int8 address) {
   output_bit(LED1, 1);
   int8 msb, lsb; 
   int16 temp; 
   i2c_start(); 
   i2c_write(BMP180_ADDR); 
    i2c_write(address); 
    i2c_start(); 
    i2c_write(BMP180_ADDR | 0x01 ); 
    msb = i2c_read(); 
    lsb = i2c_read(0); 
    i2c_stop(); 
    temp = make16(msb, lsb); 
    output_bit(LED1, 0);
    return ( temp ); 
}

void BMP180_WriteByte(int8 address, int8 data) {
   i2c_start(); 
   i2c_write(BMP180_ADDR); 
   i2c_write(address); 
   i2c_write(data); 
   i2c_stop(); 
}

//------------------ I2C TASK ---------------------------------
void task_I2Ccheck() {
   switch (i2cState)
   {
   // BMP180
   case 1: //temperature
      BMP180_alpha = (unsigned int16)BMP180_ReadInt(0xB0);
      i2cState ++;
      break; 
   case 2: 
      BMP180_fc4 = 0.000000030517578125 * BMP180_alpha;    // 1E-3 * pow2(-15) * ac4; 
      BMP180_y0 = BMP180_fc4 * 32768;                  //_c4 * pow2(15); 
      i2cState ++;
      break;       
   case 3: //temperature
      BMP180_fc5 = 0.00000019073486328125 * (unsigned int16)BMP180_ReadInt(0xB2);  // (pow2(-15)/160) * ac5; 
      i2cState ++;
      break;         
   case 4: 
      BMP180_fc6 = (float)BMP180_ReadInt(0xB4); 
      i2cState ++;
      break;
   case 5: 
      BMP180_fmc = 0.08 * (signed int16)BMP180_ReadInt(0xBC);                     // (pow2(11) / 25600) * mc; 
      i2cState ++;
      break;
   case 6: 
      BMP180_fmd = (float)BMP180_ReadInt(0xBE) / 160; 
      i2cState ++;
      break;
   case 7: 
      BMP180_x0 = (float)BMP180_ReadInt(0xAA); 
      i2cState ++;
      break;
   case 8: 
      BMP180_x1 = 0.01953125 * (signed int16)BMP180_ReadInt(0xAC);             // 160 * pow2(-13) * ac2; 
      i2cState ++;
      break;
   case 9: 
      BMP180_x2 = 0.000762939453125 * (signed int16)BMP180_ReadInt(0xB8);       // 25600 * pow2(-25) * b2; 
      i2cState ++;
      break;
   case 10: 
      BMP180_y1 = BMP180_fc4 * 0.0048828125 * (signed int16)BMP180_ReadInt(0xAE);            // 160 * pow2(-15) * ac3; 
      i2cState ++;
      break;
   case 11: 
      BMP180_y2 = BMP180_fc4 * 0.00002384185791015625 * (unsigned int16)BMP180_ReadInt(0xB6);   // 25600 * pow2(-30) * b1; 
      i2cState ++;
      break;   
   case 12: 
      BMP180_WriteByte(0xF4, 0x2E); 
      i2cState ++;
      break;
   case 13:
      BMP180_alpha = BMP180_ReadInt(0xF6); 
      i2cState ++;
      break;   
   case 14:
      BMP180_alpha = BMP180_fc5 * (BMP180_alpha - BMP180_fc6); 
      BMP180_Temp = BMP180_alpha + (BMP180_fmc / (BMP180_alpha + BMP180_fmd)); 
      BMP180_s = BMP180_Temp - 25;
      if (BMP180_Temp < 0) {
         unsigned int16 cv = (BMP180_Temp / -0.00390625);
         cv --;
         cv = 0xFFFF ^ cv;
        BMP180_Temp_H = cv >> 8;
        BMP180_Temp_L = cv & 255;
      } else {
         unsigned int16 cv = (BMP180_Temp / 0.00390625);
        BMP180_Temp_H = cv >> 8;
        BMP180_Temp_L = cv & 255; 
      }
      //Результат температуры
      i2cState ++;
      break;              
   case 15: //pressure
      BMP180_WriteByte(0xF4, (0x34 + (3 << 6))); 
      i2cState ++;
      break; 
   case 16:
      BMP180_pu = (256 * BMP180_ReadByte(0xF6));   
      i2cState ++;
      break; 
   case 17:
      BMP180_pu += BMP180_ReadByte(0xF7);   
      i2cState ++;
      break; 
   case 18:
      BMP180_pu += (BMP180_ReadByte(0xF8) / 256);   
      i2cState ++;
      break;       
   case 19:
      BMP180_x = BMP180_x2 * BMP180_s * BMP180_s + BMP180_x1 * BMP180_s + BMP180_x0;
      BMP180_y = BMP180_y2 * BMP180_s * BMP180_s + BMP180_y1 * BMP180_s + BMP180_y0;
      BMP180_z = ((float)BMP180_pu - BMP180_x) / BMP180_y;     
      i2cState ++;
      break; 
   case 20:
      BMP180_Press = (0.000004421 * BMP180_z * BMP180_z + 0.992984 * BMP180_z + 2.364375) * 100; 
      //BMP180_Press += 1.5;  
      //Результат давление
      i2cState ++;
         
      break; 

   case 30: i2cState = 0;

   default: i2cState ++;
      break;
   }
}